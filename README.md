## AURIGA TEST

### **Task 1**
The following code compiles on Linux. It has a number of problems, however. Please locate as many of those problems as you are able and provide your recommendations regarding how they can be resolved...  
**Answer:**  
See Task1.c
  
### **Task 2**
By default, Linux builds user-space programs as dynamically linked applications. Please describe scenarios where you would change the default behavior to compile your application as a statically linked one.  
**Answer:**  
It's necessary to change linking behavior to a statically linked in a case you don't have a convinient way to place shared objects.  
For example you can cross compile your application to run it in embedded system which don't have a file system. In this situation there's no way for you to link your app dynamically.

### **Task 3**  
Develop your version of the ls utility. Only the 'ls -l' functionality is needed.  
**Answer:**  
Not answered yet

### **Task 4**  
Please explain, at a very high-level, how a Unix OS, such as Linux, implements the break-point feature in a debugger.  
**Answer:**  
Not answered yet

### **Task 5**  
Suppose you debug a Linux application and put a break-point into a function defined by a dynamically-linked library used by the application. Will that break-point stop any other application that makes use of the same library?  
**Answer:**  
No, it's not going to affect other applications.

### **Task 6**  
Please translate in English. Don’t metaphrase, just put the idea as you see it into English.

Если пользовательский процесс начал операцию ввода-вывода с устройством, и остановился, ожидая окончания операции в устройстве, то операция в устройстве закончится во время работы какого-то другого процесса в системе. Аппаратное прерывание о завершении операции будет обработано операционной системой. Так как на обработку прерывания требуется некоторое время, то текущий процесс в однопроцессорной системе будет приостановлен. Таким образом, любой процесс в системе непредсказуемо влияет на производительность других процессов независимо от их приоритетов.  
**Answer:**  
User processes can unpredictably affect each other performance by triggering hardware interrupts which consume CPU's resources and cause processes to stay in waiting state. This behavior occurs, in particular, on single-core processors.





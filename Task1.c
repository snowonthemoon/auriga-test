#include <stdio.h>
#include <stdlib.h>

/**
 * Task:
 * The following code compiles on Linux. It has a number of problems, however. 
 * Please locate as many of those problems as you are able and provide your recommendations regarding how they can be resolved.
 * 
 * Commentary:
 * I implemented a solutions to bugs I had found in already existing functions as a additional functions with the "_correct" added to an original functions names.
 * More than this I put a couple of commentaries to mark some problems at the level of a module as a whole and suggest a possible solution to them too.
*/


//========================================================================================================================

typedef struct list_s
{
    struct list_s *next; /* NULL for the last item in a list */
    int data;
}
list_t;

//========================================================================================================================

/* Counts the number of items in a list.
	*/
int count_list_items(const list_t *head) {
	if (head->next) {
		return count_list_items(head->next) + 1;
	} else {
		return 1;
	}
}

int count_list_items_correct(const list_t *head) {
	// nullptr check
	if(!head) return NULL;

	if (head->next) {
		return count_list_items_correct(head->next) + 1;
	} else {
		return 1;
	}
}

//========================================================================================================================

/* Inserts a new list item after the one specified as the argument.
	*/
void insert_next_to_list(list_t *item, int data) {
	// The line below overrides item->next with the pointer, gained from malloc, and then overrides item->next->next with the same value
	// So here we have a situation, when a pointer points to an object being a part of its object 
	(item->next = malloc(sizeof(list_t)))->next = item->next;					
	item->next->data = data;
}

void insert_next_to_list_correct(list_t *item, int data) {
	// nullptr check
	if(!item) return;

	// The solution is to use temporary variable to store a previous pointer 
	struct list_s *temp = item->next;
	(item->next = malloc(sizeof(list_t)))->next = temp;					
	item->next->data = data;
}

//========================================================================================================================

/* Removes an item following the one specificed as the argument.
	*/
void remove_next_from_list(list_t *item) {
	if (item->next) {
		// Here we are freeing the dynamic memory and after that trying to override the item->next pointer with the item->next->next pointer stored in memory we just released
		// which causes segmentation fault
		free(item->next);
		item->next = item->next->next;
	}
}

void remove_next_from_list_correct(list_t *item) {
	// nullptr check
	if(!item) return;

	if (item->next) {
		// The solution is similar to previous one: to use a temorary variable
		struct list_s *temp = item->next->next;
		free(item->next);
		item->next = temp;
	}
}

//========================================================================================================================

	/* Returns item data as text.
	 */
char *item_data(const list_t *list) {
	char buf[12];

	sprintf(buf, "%d", list->data);
	// It's not a correct way to return a buffer from a function
	// Buffer "buf" is local, so it's being destroyed when function item_data ends its processing and the return value is going to be a nullptr
	return buf;
}

// The correct way is to add a pointer to char buffer as a second argument, but you should be careful for this buffer size to be enough to sprintf into it correctly
void item_data_correct(const list_t *list, char * const buf) {
	// nullptr check
	if(!list) return;

	sprintf(buf, "%d", list->data);
}


//=======================================================================================================================
// There are other several reasons why this realization of linked list isn't a good one:

// 1. Memory leaks are possible
// We can create a local object list_t, insert data to it, do some necessary actions and then leave a scope without removing an inserted data 
// It's hard to avoid a mistakes here at all, 'cause it's still going to be too much stuff on a user
// But one of the possible solutions is creating a list_t objects via a special function and destroying list_t objects via another special function when they are meant to be destroyed

list_t *create_new_list_t(void) {
	return malloc(sizeof(list_t));
}

void destroy_list_t(list_t *list) {
	while(list->next) {
		remove_next_from_list_correct(list);
	}
	free(list);
}

// 2. Another problem is a lack of convinient way to iterate the list object
// It's obviously preferable to get the data by the nubmer of element, ecpecially when we have too many data in list_t object
// This way of getting data will allow user to decrease a number of faults

list_t *get_item(list_t *list, size_t num) {
	if(!list) return NULL;

	while(num--) {
		if(list->next) {
			list = list->next;
		}
		else {
			return NULL;
		}
	}
	return list;
}

// 3. And the last common problem is all the functions get a pointer to list_t as a first argument and don't check if it's valid

//========================================================================================================================


static void print_list(list_t *list) {
	printf("Print list\n");
	size_t size = (size_t)count_list_items(list);
	printf("size = %lu\n", size);

	for(size_t i = 0; i < size; i++) {
		list_t *lst = get_item(list, i);
		char buf[16] = {0};
		item_data_correct(lst, buf);
		printf("data = %d, text data = %s\n", lst->data, buf);
	}

}

void main(void) {
	
	list_t *linked_list = create_new_list_t();
	linked_list->data = 0;
	insert_next_to_list_correct(linked_list, 777);
	insert_next_to_list_correct(linked_list, 666);
	insert_next_to_list_correct(linked_list, 555);
	insert_next_to_list_correct(linked_list, 444);
	insert_next_to_list_correct(linked_list, 333);
	insert_next_to_list_correct(linked_list, 222);
	insert_next_to_list_correct(linked_list, 111);

	print_list(linked_list);

	remove_next_from_list_correct(get_item(linked_list, 9));
	remove_next_from_list_correct(get_item(linked_list, 7));
	remove_next_from_list_correct(get_item(linked_list, 5));
	remove_next_from_list_correct(get_item(linked_list, 3));
	remove_next_from_list_correct(get_item(linked_list, 1));

	print_list(linked_list);

	destroy_list_t(linked_list);
}
